﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository
            , IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<EmployeeResponse> CreateEmployeesAsync(EmployeeRequest employeeRequest)
        {
            var roles = await GetRoleListAsync();
            Employee employee = new Employee()
            {
                Id = Guid.NewGuid(),
                Email = employeeRequest.Email,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Roles = new List<Role>()
                {
                    roles.FirstOrDefault(x => x.Name == employeeRequest.Role) ?? roles.FirstOrDefault(x => x.Name == "PartnerManager")
                },
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };
            var newEmployee = await _employeeRepository.CreateAsync(employee);

            var employeeModel = new EmployeeResponse()
            {
                Id = newEmployee.Id,
                Email = newEmployee.Email,
                Roles = newEmployee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = newEmployee.FullName,
                AppliedPromocodesCount = newEmployee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee != null)
            {
                var roles = await GetRoleListAsync();
                var emp = new Employee()
                {
                    Id = id,
                    Email = employeeRequest.Email ?? employee.Email,
                    FirstName = employeeRequest.FirstName ?? employee.FirstName,
                    LastName = employeeRequest.LastName ?? employee.LastName,
                    Roles = new List<Role>()
                    {
                        roles.FirstOrDefault(x => x.Name == employeeRequest.Role) ?? null
                    },
                    AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
                };

                //Если роль не найдена возвращается старая
                if (emp.Roles.FirstOrDefault() == null)
                    emp.Roles = employee.Roles;

                var editEmployee = await _employeeRepository.UpdateAsync(emp);

                var employeeModel = new EmployeeResponse()
                {
                    Id = editEmployee.Id,
                    Email = editEmployee.Email,
                    Roles = editEmployee.Roles.Select(x => new RoleItemResponse()
                    {
                        Name = x.Name,
                        Description = x.Description
                    }).ToList(),
                    FullName = editEmployee.FullName,
                    AppliedPromocodesCount = editEmployee.AppliedPromocodesCount
                };

                return Ok(employeeModel);
            }
            else
            {
                return BadRequest("Данный сотрудник не найден");
            }
        }

        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            await _employeeRepository.DeleteByIdAsync(id);
            return Ok();

        }

        private async Task<IEnumerable<Role>> GetRoleListAsync()
        {
            return await _roleRepository.GetAllAsync();
        }
    }
}