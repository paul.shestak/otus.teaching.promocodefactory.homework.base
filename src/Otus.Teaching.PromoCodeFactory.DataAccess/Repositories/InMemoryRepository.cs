﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ConcurrentDictionary<Guid, T> Data { get; set; }
        //protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new ConcurrentDictionary<Guid, T>(data.ToDictionary(d => d.Id));
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.Select(d => d.Value));
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Key == id).Value);
            //return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T data)
        {
            Data.TryAdd(data.Id, data);
            //Data = Data.Append(data);
            return Task.FromResult(data);
        }

        public Task<T> UpdateAsync(T data)
        {
            Data.TryUpdate(data.Id, data, Data.FirstOrDefault(x => x.Key == data.Id).Value);
            //Data = Data.Where(x => x.Id != data.Id);
            //Data = Data.Append(data);
            return Task.FromResult(data);
        }

        public async Task DeleteByIdAsync(Guid id)
        {
            Data.TryRemove(id, out T value);
            //Data = Data.Where(x => x.Id != id);
        }
    }
}